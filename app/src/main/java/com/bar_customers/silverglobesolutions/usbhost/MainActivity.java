package com.bar_customers.silverglobesolutions.usbhost;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements DeviceListAdapter.connectDevice {
    private static final String ACTION_USB_PERMISSION =
            "com.android.example.USB_PERMISSION";
    String TAG = MainActivity.this.getClass().getSimpleName();
    PendingIntent mPermissionIntent;
    RecyclerView DeviceListView;
    UsbManager manager;
    ArrayList<UsbDevice> usbDevices = new ArrayList<>();
    TextView ScrollingTextView;
    NestedScrollView scrollView;


    UsbEndpoint endpointIn;
    UsbEndpoint endpointOut;
    UsbInterface intf;
    UsbDeviceConnection connection;


    Boolean deviceDetached = false;

    Handler h = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            ScrollingTextView.setText(ScrollingTextView.getText() + "\n" + "Received Bytes: " + msg.getData().get("DATA"));


        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ScrollingTextView = (TextView) findViewById(R.id.scrollingTextView);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);


        DeviceListView = (RecyclerView) findViewById(R.id.deviceListView);
        DeviceListView.setLayoutManager(new LinearLayoutManager(this));

        manager = (UsbManager) getSystemService(Context.USB_SERVICE);


        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        registerReceiver(mUsbReceiver, filter);


    }

    @Override
    protected void onResume() {
        super.onResume();

        scanDevices();

    }


    /*---1----------------------------Check for connected host-------------------------------*/
    void scanDevices() {
        usbDevices.clear();
        DeviceListView.setAdapter(null);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        if (deviceIterator.hasNext()) {
            while (deviceIterator.hasNext()) {
                UsbDevice device = deviceIterator.next();
                usbDevices.add(device);
                DeviceListView.setAdapter(new DeviceListAdapter(usbDevices, MainActivity.this, this));


            }
        } else {
            DeviceListView.setAdapter(null);
        }

    }
 /*----3---------------------------Connect with USB Host and initialize connections-------------------------------*/

    void initUsbHost(UsbDevice device) {

        //Change endpoint types as per usb host
//        Log.d("TYPE", String.valueOf(intf.getEndpoint(i).getType()));
//        Log.d("TYPE", String.valueOf(intf.getEndpoint(i).getDirection()));



        intf = device.getInterface(0);
        deviceDetached = false;
        for (int i = 0; i < intf.getEndpointCount(); i++) {
            if (intf.getEndpoint(i).getDirection() == UsbConstants.USB_DIR_IN) {
                endpointIn = intf.getEndpoint(i);
            }

            if (intf.getEndpoint(i).getDirection() == UsbConstants.USB_DIR_OUT) {
                endpointOut = intf.getEndpoint(i);
            }


        }

        connection = manager.openDevice(device);


        startReadingData(device);
    }

/*---4----------------------------Send Data To USB Host-------------------------------*/


    void sendDataToDevice(UsbDevice device){

        String tOut = "SEND THIS DATA";
        byte[] bytesOut = tOut.getBytes(); //convert String to byte[]
        int usbResult = connection.bulkTransfer(
                endpointOut, bytesOut, bytesOut.length, 0);
    }


/*----5---------------------------Receive Data from USB Host-------------------------------*/


    void startReadingData(UsbDevice device) {
        boolean forceClaim = true;


        connection.claimInterface(intf, forceClaim);


        final byte[] readBytes = new byte[endpointIn.getMaxPacketSize()];
        if (scrollView.getVisibility() == View.GONE) {
            scrollView.setVisibility(View.VISIBLE);

        }

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {


                    if (deviceDetached)
                        break;

                    int recvBytes = connection.bulkTransfer(endpointIn, readBytes, readBytes.length, 0);


                    if (recvBytes > 0) {

                        try {

                            Message m = new Message();
                            Bundle b = new Bundle();
                            b.putString("DATA", Arrays.toString(readBytes));
                            m.setData(b);
                            h.sendMessage(m);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Log.d("trebla", "Did not get any data: " + recvBytes);


                    }

                }
            }
        });
        t.start();

    }




    /*-----6--------------------------Reset all connections-------------------------------*/


    public void DeviceDetached() {
        deviceDetached = true;
        ScrollingTextView.setText("Receiving Data");
        if (scrollView.getVisibility() == View.VISIBLE) {
            scrollView.setVisibility(View.GONE);


        }
        if (connection != null) {
            if (intf != null) {
                connection.releaseInterface(intf);
                intf = null;
            }
            connection.close();
            connection = null;
        }


        endpointIn = null;
        endpointOut = null;

        scanDevices();
    }



     /*---2----------------------------Broadcast Receiver to connect new Usb Host-------------------------------*/

    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();


            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                if (device != null) {
                    DeviceDetached();

                }
            }

            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {

                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);


                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            initUsbHost(device);


                        }
                    } else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    };





    @Override
    public void onBackPressed() {
        if (scrollView.getVisibility() == View.VISIBLE) {
            scrollView.setVisibility(View.GONE);


        }

        DeviceDetached();
    }



     /*-------------------------------Connect with Usb Host-------------------------------*/

    @Override
    public void connectDevice(UsbDevice device) {
        manager.requestPermission(device, mPermissionIntent);
    }
}
