package com.bar_customers.silverglobesolutions.usbhost;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by silverglobesolutions on 18/11/17.
 */

public class DeviceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

public interface connectDevice{
    public void connectDevice(UsbDevice device);
}


connectDevice callback;



    ArrayList<UsbDevice>usbdevices = new ArrayList<>();
    Context context;

    public DeviceListAdapter(ArrayList<UsbDevice> usbdevices, Context context,connectDevice callback) {
        this.usbdevices = usbdevices;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.device_list_layout,parent,false);
        return new GenricView(v);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final UsbDevice usbDevice = usbdevices.get(position);


       if(holder instanceof GenricView) {


           ((GenricView) holder).prp1.setText("DEVICE NAME:: "+usbDevice.getDeviceName());
           ((GenricView) holder).prp2.setText("VENDOR ID:: "+usbDevice.getVendorId());

           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
               ((GenricView) holder).prp3.setText("PRODUCT NAME:: "+usbDevice.getProductName());
               ((GenricView) holder).prp4.setText("MANUFACTURER NAME:: "+usbDevice.getManufacturerName());
               ((GenricView) holder).prp5.setText("SERIAL NUMBER :: "+usbDevice.getSerialNumber());
               ((GenricView) holder).prp6.setText("DEVICE NAME:: "+usbDevice.getSerialNumber());
           }

           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
               ((GenricView) holder).prp7.setText("VERSION NAME:: "+usbDevice.getVersion());
           }
           ((GenricView) holder).prp8.setText("DEVICE ID:: "+usbDevice.getDeviceId());
           ((GenricView) holder).prp9.setText("INTERFACE COUNT:: "+usbDevice.getInterfaceCount());
           ((GenricView) holder).prp10.setText("PRODUCT ID:: "+usbDevice.getProductId());


           ((GenricView) holder).parent.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   callback.connectDevice(usbDevice);
               }
           });
       }

    }

    @Override
    public int getItemCount() {
        return usbdevices.size();
    }

    public class GenricView extends RecyclerView.ViewHolder {
        TextView prp1, prp2, prp3, prp4, prp5, prp6, prp7, prp8, prp9, prp10;
        LinearLayout parent;

        public GenricView(View itemView) {
            super(itemView);
            parent = (LinearLayout)itemView.findViewById(R.id.parentLayout);

            prp1 = (TextView) itemView.findViewById(R.id.prp1);
            prp2 = (TextView) itemView.findViewById(R.id.prp2);
            prp3 = (TextView) itemView.findViewById(R.id.prp3);
            prp4 = (TextView) itemView.findViewById(R.id.prp4);
            prp5 = (TextView) itemView.findViewById(R.id.prp5);
            prp6 = (TextView) itemView.findViewById(R.id.prp6);
            prp7 = (TextView) itemView.findViewById(R.id.prp7);
            prp8 = (TextView) itemView.findViewById(R.id.prp8);
            prp9 = (TextView) itemView.findViewById(R.id.prp9);
            prp10 = (TextView) itemView.findViewById(R.id.prp10);

        }
    }

}
